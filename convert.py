# SPDX-FileCopyrightText: 2016-2025, Thomas Larsson
#
# SPDX-License-Identifier: GPL-2.0-or-later

import bpy
from mathutils import *
from .error import *
from .utils import *
from .fileutils import SingleFile, JsonFile, JsonExportFile, DF

#-------------------------------------------------------------
#   Load pose
#-------------------------------------------------------------

def getCharacterFromRig(rig):
    if dazRna(rig).DazMesh:
        char = dazRna(rig).DazMesh.lower().replace("-","_").replace("genesis", "genesis_")
        if char[-1] == "_":
            char = char[:-1]
        print("Character: %s" % char)
        return char
    else:
        return None


def loadPose(context, rig, entry):

    def getBoneName(bname, bones):
        if bname in bones.keys():
            return bname
        elif isDrvBone(bname):
            bname = baseBone(bname)
            if bname in bones.keys():
                return bname
        elif (bname[-4:] == "Copy" and
              bname[:-4] in bones.keys()):
            return bname[:-4]
        return None

    def loadBonePose(context, pb, pose):
        pbname = getBoneName(pb.name, pose)
        if pbname and pb.name[:-4] != "Copy":
            rot, dazRna(pb.bone).DazOrient, dazRna(pb).DazRotMode = pose[pbname]
            euler = Euler(rot)
            mat = euler.to_matrix()
            rmat = pb.bone.matrix_local.to_3x3()
            if pb.parent:
                par = pb.parent
                rmat = par.bone.matrix_local.to_3x3().inverted() @ rmat
                mat = par.matrix.to_3x3().inverted() @ mat
            bmat = rmat.inverted() @ mat
            pb.matrix_basis = bmat.to_4x4()
            for n in range(3):
                if pb.lock_rotation[n]:
                    pb.rotation_euler[n] = 0
            updateScene(context)

        if pb.name != "head":
            for child in pb.children:
                loadBonePose(context, child, pose)

    roots = [pb for pb in rig.pose.bones if pb.parent is None]
    loadBonePose(context, roots[0], entry["pose"])

#-------------------------------------------------------------
#   Optimize pose for IK
#-------------------------------------------------------------

def optimizePose(context, useApplyRestPose):
    from .apply import applyRestPoses
    rig = context.object
    char = getCharacterFromRig(rig)
    if char is None:
        reportError("Could not optimize pose because the character was not recognized.")
        return
    entry = DF.loadEntry(char, "ikposes")
    loadPose(context, rig, entry)
    if useApplyRestPose:
        applyRestPoses(context, rig)

#-------------------------------------------------------------
#   Bone conversion
#-------------------------------------------------------------

def getConverter(srctype, trg):
    if srctype == "genesis8":
        srctype = "genesis3"
    elif srctype == "genesis":
        srctype = "genesis1"
    trgtype = dazRna(trg).DazRig
    if trgtype[-7:] == ".suffix":
        trgtype = trgtype[:-7]
    if trgtype == "genesis8":
        trgtype = "genesis3"
    elif trgtype == "genesis":
        trgtype = "genesis1"

    if srctype == "" or trgtype == "":
        return {},[]
    if (srctype in DF.TwistBones.keys() and
        trgtype not in DF.TwistBones.keys()):
        twists = DF.TwistBones[srctype]
    else:
        twists = []

    if srctype == trgtype:
        return {},twists
    if trgtype.startswith(("mhx", "rigify")):
        file = "genesis-%s" % trgtype
    elif trgtype == "genesis9":
        file = "genesis1238-genesis9"
    else:
        file = "%s-%s" % (srctype, trgtype)
    conv = DF.loadEntry(file, "converters")
    if not conv:
        print("No converter", srctype, dazRna(trg).DazRig)
    return conv, twists

